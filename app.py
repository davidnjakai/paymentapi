import sqlite3
import datetime
from flask import g, Flask, request
import hashlib, binascii, os
import re
import random, string
# from db_utils import get_db_cursor, get_db_connection

app = Flask(__name__)

def generate_receipt():
    return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(10))

DATABASE_NAME = 'customers.db'
BUSINESS_ACCOUNT_NO = 111
TEST_PASS = 1234

def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')

TEST_HASHED_PASS = hash_password(str(TEST_PASS))

def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'), salt.encode('ascii'), 100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password

def get_db_connection(db_name):
    return sqlite3.connect(db_name)

def get_db_cursor(connection):
    return connection.cursor()

def create_initial_tables(cursor):
    cursor.execute('''CREATE TABLE tbl_charges(charge_id integer primary key AUTOINCREMENT,min integer, max integer, withdraw_charge integer,send_to_unregistered integer, send_to_registered integer)''')

    cursor.execute('''INSERT INTO tbl_charges(min, max, withdraw_charge, send_to_unregistered, send_to_registered) VALUES (0, 1000, 0, 0, 0)''')
    cursor.execute('''INSERT INTO tbl_charges(min, max, withdraw_charge, send_to_unregistered, send_to_registered) VALUES (1001, 10000, 112, 205, 87)''')
    cursor.execute('''INSERT INTO tbl_charges(min, max, withdraw_charge, send_to_unregistered, send_to_registered) VALUES (10001, 30000, 180, 288, 102)''')
    cursor.execute('''INSERT INTO tbl_charges(min, max, withdraw_charge, send_to_registered) VALUES (30001, 50000, 270, 105)''')
    cursor.execute('''INSERT INTO tbl_charges(min, max, withdraw_charge, send_to_registered) VALUES (50001, 70000, 300, 105)''')

    cursor.execute('''CREATE TABLE tbl_business_accounts(uuid integer primary key AUTOINCREMENT, account_no integer, account_name text, status text, account_balance real, last_activity datatype, created_date text)''')

    cursor.execute('''INSERT INTO tbl_business_accounts(account_no, account_name, status, account_balance, last_activity, created_date)
    VALUES (111, "XYZacc1", "active", 1000000000, "account activation", "{current_date}")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M")))

    cursor.execute('''CREATE TABLE tbl_main_transactions(uuid integer primary key AUTOINCREMENT, request_id integer, receipt text, partya integer, partyb integer, amount integer, date text, status text, charge integer)''')

    cursor.execute('''CREATE TABLE tbl_subtransactions(uuid integer primary key AUTOINCREMENT, request_id integer, receipt text, phone integer, amount integer, amount_type text, transaction_type text)''')

    cursor.execute('''CREATE TABLE tbl_mobile_wallet_details(wallet_id integer primary key AUTOINCREMENT, surname text, firstname text, secondname text, reg_date text, account_bal real, status text, last_activity text, phone_number integer, pin text)''')

    cursor.execute('''INSERT INTO tbl_mobile_wallet_details(surname, firstname, secondname, reg_date, account_bal, status, last_activity, phone_number, pin)
    VALUES ("Kamau", "Alfred", "P", "{current_date}", 100000, "registered", "{current_date}", 712111111, "{hashed_pin}")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M"), hashed_pin=TEST_HASHED_PASS))

    cursor.execute('''INSERT INTO tbl_mobile_wallet_details(surname, firstname, secondname, reg_date, account_bal, status, last_activity, phone_number, pin)
    VALUES ("Lugo", "Benson", "Q", "{current_date}", 100000, "registered", "{current_date}", 712222222, "{hashed_pin}")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M"), hashed_pin=TEST_HASHED_PASS))

    cursor.execute('''INSERT INTO tbl_mobile_wallet_details(surname, firstname, secondname, reg_date, account_bal, status, last_activity, phone_number, pin)
    VALUES ("Mauri", "Charles", "R", "{current_date}", 100000, "unregistered", "{current_date}", 712333333, "{hashed_pin}")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M"), hashed_pin=TEST_HASHED_PASS))

    cursor.execute('''INSERT INTO tbl_mobile_wallet_details(surname, firstname, secondname, reg_date, account_bal, status, last_activity, phone_number, pin)
    VALUES ("Nola", "Derrick", "S", "{current_date}", 100000, "unregistered", "{current_date}", 712444444, "{hashed_pin}")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M"), hashed_pin=TEST_HASHED_PASS))

    cursor.execute('''INSERT INTO tbl_mobile_wallet_details(surname, firstname, secondname, reg_date, account_bal, status, last_activity, phone_number, pin)
    VALUES ("Onyango", "Eric", "T", "{current_date}", 100000, "unregistered", "{current_date}", 712555555, "{hashed_pin}")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M"), hashed_pin=TEST_HASHED_PASS))

    cursor.execute('''CREATE TABLE tbl_customer_details(customer_id integer primary key AUTOINCREMENT, surname text, firstname text, secondname text, reg_date text, id_number text, phone_number integer, status text)''')

    cursor.execute('''INSERT INTO tbl_customer_details(surname, firstname, secondname, reg_date, id_number, phone_number, status)
    VALUES ("Kamau", "Alfred", "P", "{current_date}", "2193031", 712111111, "registered")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M")))

    cursor.execute('''INSERT INTO tbl_customer_details(surname, firstname, secondname, reg_date, id_number, phone_number, status)
    VALUES ("Lugo", "Benson", "Q", "{current_date}", "2193032", 712222222, "registered")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M")))

    cursor.execute('''INSERT INTO tbl_customer_details(surname, firstname, secondname, reg_date, id_number, phone_number, status)
    VALUES ("Mauri", "Charles", "R", "{current_date}", "2193033", 712333333, "unregistered")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M")))

    cursor.execute('''INSERT INTO tbl_customer_details(surname, firstname, secondname, reg_date, id_number, phone_number, status)
    VALUES ("Nola", "Derrick", "S", "{current_date}", "2193034", 712444444, "unregistered")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M")))

    cursor.execute('''INSERT INTO tbl_customer_details(surname, firstname, secondname, reg_date, id_number, phone_number, status)
    VALUES ("Onyango", "Eric", "T", "{current_date}", "2193035", 712555555, "unregistered")'''.format(current_date=datetime.date.today().strftime("%Y%m%d%H%M")))

app = Flask(__name__)

def initialize_app():
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    create_initial_tables(cur)

    conn.commit()
    conn.close()

def hasEnoughFunds(partya, partyb, amount_to_transfer):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    cur.execute('''SELECT account_bal FROM tbl_mobile_wallet_details
    WHERE phone_number = {phone_number}'''.format(phone_number=str(partya)[-9:]))

    rows = cur.fetchall()

    balance = rows[0][0]

    transer_charge = getCharges(amount_to_transfer, isRegistered(partyb))

    conn.close()

    return amount_to_transfer + transer_charge <= balance

def updateMainTransaction(partya, partyb, amount, charge, receipt):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    if amount < 0:
        transaction_type = 'debit'
    else:
        transaction_type = 'credit'

    cur.execute('''INSERT INTO tbl_main_transactions(receipt, partya, partyb, amount, date, charge)
    VALUES ("{receipt}", {partya}, {partyb}, {amount}, "{current_date}", {charge})'''.format(receipt=receipt, partya=str(partya)[-9:], partyb=str(partyb)[-9:], amount=amount, current_date=datetime.date.today().strftime("%Y%m%d%H%M"), charge=charge))

    conn.commit()
    conn.close()

def updateSubTransaction(phone_number, amount, amount_type, receipt):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    if amount < 0:
        transaction_type = 'debit'
    else:
        transaction_type = 'credit'

    print('transaction_type is ' + transaction_type)

    if phone_number == BUSINESS_ACCOUNT_NO:
        phone_number = str(BUSINESS_ACCOUNT_NO)
    else:
        phone_number = str(phone_number)[-9:]

    cur.execute('''INSERT INTO tbl_subtransactions(request_id, phone, amount, amount_type, transaction_type, receipt)
    VALUES ("{request_id}", {phone_number}, {amount}, "{amount_type}", "{transaction_type}", "{receipt}")'''.format(receipt=receipt, request_id=datetime.date.today().strftime("%Y%m%d%H%M"), phone_number=phone_number, amount=amount, amount_type=amount_type, transaction_type=transaction_type))

    print('inserted to tbl_subtransactions.')

    conn.commit()
    conn.close()

def updateCustomerBalance(phone_number, amount, charge, transaction_type):
    print('updating customer balance...')
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    total = amount + charge
    print('total charge: ' + str(total))

    cur.execute('''UPDATE tbl_mobile_wallet_details
    SET account_bal=account_bal+({amount})
    WHERE phone_number={phone_number}'''.format(amount=total, phone_number=str(phone_number)[-9:]))

    conn.commit()
    conn.close()

def isRegistered(phone_number):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    cur.execute('''SELECT status FROM tbl_mobile_wallet_details
    WHERE phone_number = {phone_number}'''.format(phone_number=str(phone_number)[-9:]))

    rows = cur.fetchall()

    conn.close()

    print(str(phone_number) + ' registered?: ' + str(rows[0][0]) == 'registered')
    return rows[0][0] == 'registered'

def getCharges(amount_to_transfer, registered=True):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    if registered:
        column_name = 'send_to_registered'
    else:
        column_name = 'send_to_unregistered'

    cur.execute('''SELECT {column_name} FROM tbl_charges
    WHERE {amount_to_transfer} >= min
    AND {amount_to_transfer} <= max'''.format(amount_to_transfer=amount_to_transfer, column_name=column_name))

    rows = cur.fetchall()

    conn.close()

    return int(rows[0][0])

def creditCharges(credit_amount):
    print('updating business account (+' + str(credit_amount) + str(')'))
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    cur.execute('''UPDATE tbl_business_accounts
    SET account_balance=account_balance+({credit_amount})
    WHERE account_no={business_acccount_no}'''.format(credit_amount=credit_amount, business_acccount_no=BUSINESS_ACCOUNT_NO))

    conn.commit()
    conn.close()

def pinIsValid(phone, provided_pin):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    cur.execute('''SELECT pin FROM tbl_mobile_wallet_details
    WHERE phone_number = {phone_number}'''.format(phone_number=str(phone)[-9:]))

    rows = cur.fetchall()

    conn.close()

    stored_password = rows[0][0]

    return verify_password(stored_password, provided_pin)

def validInputs(partya, partyb, amount_to_transfer):
    pattern1 = r'7[0-9]{8}'
    pattern2 = r'07[0-9]{8}'
    pattern3 = r'2547[0-9]{8}'

    if True not in [bool(re.match(x, str(partyb))) for x in [pattern1, pattern2, pattern3]]:
        return [False, 'wrong number format']

    if amount_to_transfer > 70000:
        return [False, 'max allowed is 70000']
    if not isRegistered(partyb) and amount_to_transfer > 30000:
        return [False, 'max allowed for unregistered users is 30000']

    return [True, 'success']

def c2ctransfer(partya, partyb, amount, auth_string):

    success, message = validInputs(partya, partyb, amount)

    if not success:
        print(message)
        return message

    if not pinIsValid(partya, auth_string):
        print("invalid pin!")
        return "invalid pin!"
    if hasEnoughFunds(partya, partyb, amount):
        receipt = generate_receipt()
        print('receipt ' + receipt + ' generated.')
        print('amount: ' + str(amount))
        credit_charge = getCharges(amount, isRegistered(partyb))
        print('credit charge is ' + str(credit_charge))
        updateCustomerBalance(partya, amount * -1, credit_charge * -1, 'debit')
        updateSubTransaction(partya, amount * -1, 'transaction_amount', receipt)
        updateCustomerBalance(partyb, amount, 0, 'credit')
        updateSubTransaction(partyb, amount, 'transaction_amount', receipt)
        creditCharges(credit_charge)
        updateSubTransaction(BUSINESS_ACCOUNT_NO, credit_charge, 'charge', receipt)

        updateMainTransaction(partya, partyb, amount, credit_charge, receipt)

        print('transaction complete')

    else:
        return "Insufficient Balance!"

def transaction_details(receipt):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    cur.execute('''SELECT phone, amount, amount_type, transaction_type
    FROM tbl_subtransactions
    WHERE receipt = "{receipt}"'''.format(receipt=receipt))

    rows = cur.fetchall()

    response_dict = {
        'transactions': [],
    }

    for row in rows:
        phone, amount, amount_type, transaction_type = row

        response_dict['transactions'].append({
            'phone': phone,
            'amount': amount,
            'amount_type': amount_type,
            'transaction_type': transaction_type
        })

    cur.execute('''SELECT date
    FROM tbl_main_transactions
    WHERE receipt = "{receipt}"'''.format(receipt=receipt))

    rows = cur.fetchall()

    transaction_date = rows[0][0]

    response_dict['transaction_date'] = transaction_date
    response_dict['receipt'] = receipt

    conn.close()

    return response_dict

# @app.route('/transaction_history', methods=['POST'])
def transaction_history(start_date, end_date, phone):
    conn = get_db_connection(DATABASE_NAME)
    cur = get_db_cursor(conn)

    cur.execute('''SELECT receipt, partyb, amount, date, charge
    FROM tbl_main_transactions
    WHERE date BETWEEN "{start_date}" AND "{end_date}"
    AND partya = {phone}'''.format(start_date=start_date, end_date=end_date, phone=str(phone)[-9:]))

    rows = cur.fetchall()

    response_dict = {
        'transactions': [],
    }

    for row in rows:
        receipt, partyb, amount, date, charge = row

        response_dict['transactions'].append({
            'receipt': receipt,
            'partyb': partyb,
            'amount': amount,
            'date': date,
            'charge': charge
        })

    response_dict['phone_number'] = phone

    conn.close()

    return response_dict

initialize_app()

#test payment
c2ctransfer(254712111111, 254712222222, 10000, '1234')

# if __name__ == '__main__':
#     app.run(debug=True)
